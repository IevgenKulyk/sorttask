//Given two arrays of integers, please write a function
//    that returns all elements present in one of the two arrays but not both.
//    E.g. f([ 1, 3, 5 ], [ 1, 2, 4, 5 ]) -> [ 2, 3, 4 ]
//Please use C++11.

#include <iostream>
#include <vector>
#include <algorithm>

std::vector<int> merge_integers(const std::vector<int>& a,const std::vector<int>& b)
{
    const auto total_len = a.size() + b.size();
    std::vector<int> result(total_len);

    unsigned int i = 0, j = 0, n = 0;
    while (i < a.size() && j < b.size())
    {
        if (a[i] < b[j])
        {
            result[n++] = a[i++];
        }
        else if (a[i] > b[j])
        {
            result[n++] = b[j++];
        }
        else if (a[i] == b[j])
        {
            i++;
            j++;
        }
    }

    if (i >= a.size())
    {
        std::copy(b.begin() + j,b.end(),result.begin() + n);
        n += b.size() - j;
    }
    else if (j >= b.size())
    {
        std::copy(a.begin() + i,a.end(),result.begin() + n);
        n += a.size() - i;
    }

    result.resize(n);
    return result;
}

int main()
{
    std::vector<int> A = {1, 3, 5 };
    std::vector<int> B = {1, 2, 4, 5};

    std::vector<int> result = std::move( merge_integers(A,B) );
    for (auto el:result)
    {
        std::cout << el << std::endl;
    }
    return 0;
}